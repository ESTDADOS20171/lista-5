package heap

object Questao1 extends App {

  var heap = new HeapMax[Int](10, 0);

  println("Heap vazio (posicoes preenchidas com valores default)");
  println(heap);

  println("Inserir 2");
  heap.insere(2);
  println(heap);

  println("Inserir 3");
  heap.insere(3);
  println(heap);

  println("Inserir 4");
  heap.insere(4);
  println(heap);

  println();
  println("Remover elemento do topo");
  heap.remove();
  println(heap);

  println("Inserir 1");
  heap.insere(1);
  println(heap);

  println();
  println("Buscar elemento 2");
  heap.buscar(2) match {
    case Some(pos) => println("Achou 2 na posicao: " + pos);
    case None => println("Nao encontrou 2");
  }

  println("Buscar elemento 5");
  heap.buscar(5) match {
    case Some(pos) => println("Achou 5 na posicao: " + pos);
    case None => println("Nao encontrou 5");
  }

  println();
  println("Alterar o elemento na posicao 1 do heap para 7");
  heap.alterar(1, 7);
  println(heap);

  println("Alterar o elemento na posicao 2 do heap para 15");
  heap.alterar(2, 15);
  println(heap);

  println("Inserir 5");
  heap.insere(5);
  println(heap);

  println();
  println("Buscar elemento 5");
  heap.buscar(5) match {
    case Some(pos) => println("Achou 5 na posicao: " + pos);
    case None => println("Nao encontrou 5");
  }
  
  println();
  println("Remover elemento do topo.");
  heap.remove();
  println(heap);
  
  
   println();
  println("Buscar elemento 15");
  heap.buscar(15) match {
    case Some(pos) => println("Achou 15 na posicao: " + pos);
    case None => println("Nao encontrou 15");
  }
  
}