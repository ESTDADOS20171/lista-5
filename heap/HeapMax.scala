package heap

class HeapMax[E <% Ordered[E]: ClassManifest](n: Int, e: E) {

  private var vetor: Array[E] = new Array[E](n);
  private var pos: Int = 0;

  for (i <- 0 until n) {
    vetor(i) = e;
  }

  private def pai(i: Int): Int = scala.math.floor((i - 1) / 2).toInt;

  private def esq(i: Int): Int = 2 * i + 1;

  private def dir(i: Int): Int = 2 * i + 1;

  private def swap(i: Int, j: Int) = {
    var e: E = vetor(i);
    vetor(i) = vetor(j);
    vetor(j) = e;
  }

  private def corrigeCima(pos: Int) {
    var posCorrecao = pos;
    while (posCorrecao > 0) {
      if (vetor(pai(posCorrecao)) < vetor(posCorrecao)) {
        swap(pos, pai(posCorrecao));
      } else {
        return ;
      }
      posCorrecao = pai(posCorrecao);
    }
  }

  private def corrigeBaixo() {

    var posCorrecao = 0;

    while (pai(posCorrecao) < pos) {
      var filho_esq = esq(posCorrecao);
      var filho_dir = dir(posCorrecao);
      var filho = filho_dir;

      if (filho_dir >= pos) { filho_dir = filho_esq; }

      if (vetor(filho_esq) > vetor(filho_dir)) {
        filho = filho_esq;
      }

      if (vetor(posCorrecao) < vetor(filho)) {
        swap(posCorrecao, filho);
      } else {
        return ;
      }
      posCorrecao = filho;
    }

  }

  def insere(valor: E): Option[E] = {
    if (pos == vetor.length - 1) {
      return None;
    }

    vetor(pos) = valor;
    corrigeCima(pos);
    pos += 1;
    return Some(valor);

  }

  def remove(): Option[E] = {
    if (pos > 0) {
      var topo = vetor(0);
      vetor(0) = vetor(pos - 1);
      pos -= 1;

      corrigeBaixo();
      vetor(pos) = e;
      return Some(topo);
    }
    return None;

  }

  def buscar(valor: E): Option[Int] = {
    var posBusca = 0;
    while (posBusca < pos) {

      if (valor == vetor(posBusca)) {

        return Some(posBusca);

      } else if (valor > vetor(esq(posBusca)) &&
        valor > vetor(dir(posBusca))) {
        return None;

      }
      posBusca += 1;

    }

    return None;

  }

  def alterar(posicao: Int, valor: E): Option[Int] = {

    if (posicao < 0 && posicao > pos) {
      return None;
    }

    vetor(posicao) = valor;
    if (valor > vetor(pai(posicao))) {
      corrigeCima(posicao);
      return buscar(valor);
    } else if (valor < vetor(esq(posicao)) || valor < vetor(dir(posicao))) {
      corrigeBaixo();
      return buscar(valor);
    }

    return None;
  }

  def liberar = { vetor = null; pos = -1 };

  override def toString = vetor.mkString("[", ", ", "]");

}